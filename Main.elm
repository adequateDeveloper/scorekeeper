module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onSubmit, onInput, onClick)


-- Model


type alias Player =
    { id : Int
    , name : String
    , points : Int
    }


type alias Play =
    { id : Int
    , playerId : Int
    , name : String
    , points : Int
    }


type alias Model =
    { currentPlayerId : Maybe Int {- Determines whether in Add or Edit mode -}
    , inputName : String {- Add/Edit Player input field -}
    , players : List Player
    , plays : List Play
    }


initModel : Model
initModel =
    { currentPlayerId = Nothing
    , inputName = ""
    , players = []
    , plays = []
    }



-- Update
{- Messages:
   Save       - when the form Save button is clicked
   Cancel     - when the form Cancel button is clicked
   Edit       - when an edit player icon (pencil button) is clicked
   Input      - when a player name is entered in the Add/Edit Player input field
   DeletePlay - when the red X icon button is clicked
   Score      - when a score point button (2pt/3pt) for a player is clicked
-}


type Msg
    = Save
    | Cancel
    | Edit Player
    | Input String
    | DeletePlay Play
    | Score Player Int


update : Msg -> Model -> Model
update msg model =
    case msg of
        Save ->
            if String.isEmpty model.inputName then
                Debug.log "Save Msg - player name empty" model
            else
                Debug.log "Save Msg - player name filled" save model

        Cancel ->
            {- Set currentPlayerId = Nothing in case mode = edit -}
            Debug.log "Cancel Msg" { model | inputName = "", currentPlayerId = Nothing }

        Edit player ->
            Debug.log "Edit Msg" { model | inputName = player.name, currentPlayerId = Just player.id }

        Input name ->
            {- Debug.log the causes model to be displayed in both elm debug window and browser debug console tab -}
            Debug.log "Input Msg" { model | inputName = name }

        DeletePlay play ->
            Debug.log "Delete Msg" deletePlay model play

        Score player points ->
            Debug.log "Score Msg" score model player points



{- catch-all condition used during development
   _ ->
     Debug.log "Unknown Msg" model
-}


score : Model -> Player -> Int -> Model
score model scorer points =
    let
        newPlayers =
            List.map
                (\player ->
                    if player.id == scorer.id then
                        { player | points = player.points + points }
                    else
                        player
                )
                model.players

        play =
            Play (List.length model.plays) scorer.id scorer.name points
    in
        { model | players = newPlayers, plays = play :: model.plays }


deletePlay : Model -> Play -> Model
deletePlay model play =
    {-
       1. remove the selected play from the plays List
       2. update the player's points total in the player list
    -}
    let
        newPlays =
            List.filter (\aPlay -> aPlay.id /= play.id) model.plays

        newPlayers =
            List.map
                (\player ->
                    if player.id == play.playerId then
                        { player | points = player.points - (1 * play.points) }
                    else
                        player
                )
                model.players
    in
        { model | plays = newPlays, players = newPlayers }


save : Model -> Model
save model =
    {- handle both add new player and edit existing player depending on current mode -}
    case model.currentPlayerId of
        Just id ->
            {- edit mode -}
            Debug.log "currentPlayerId Exists - edit called" edit model id

        Nothing ->
            {- add mode -}
            Debug.log "currentPlayerId Missing - add called" add model


add : Model -> Model
add model =
    let
        player =
            {- using the length of the players list to generate the next playerId -}
            Player (List.length model.players) model.inputName 0

        newPlayers =
            player :: model.players
    in
        { model | players = newPlayers, inputName = "" }


edit : Model -> Int -> Model
edit model id =
    let
        newPlayers =
            List.map
                (\player ->
                    if player.id == id then
                        { player | name = model.inputName }
                    else
                        player
                )
                model.players

        newPlays =
            List.map
                (\play ->
                    if play.playerId == id then
                        { play | name = model.inputName }
                    else
                        play
                )
                model.plays
    in
        { model
            | players = newPlayers
            , plays = newPlays
            , inputName = ""
            , currentPlayerId = Nothing
        }



-- View


viewPlayerSection : Model -> Html Msg
viewPlayerSection model =
    div []
        [ viewPlayerListHeader
        , viewPlayerList model
        , viewPointTotal model
        ]


viewPlayerListHeader : Html Msg
viewPlayerListHeader =
    header []
        [ div [] [ text "Name" ]
        , div [] [ text "Points" ]
        ]


viewPlayerList : Model -> Html Msg
viewPlayerList model =
    -- ul []
    -- (List.map player model.players)
    model.players
        |> List.sortBy .name
        |> List.map viewPlayer
        |> ul []


viewPlayer : Player -> Html Msg
viewPlayer player =
    li []
        [ i [ class "edit", onClick (Edit player) ] []
        , div [] [ text player.name ]
        , button
            [ type_ "button", onClick (Score player 2) ]
            [ text "2pt" ]
        , button
            [ type_ "button", onClick (Score player 3) ]
            [ text "3pt" ]
        , div [] [ text <| toString player.points ]
        ]


viewPointTotal : Model -> Html Msg
viewPointTotal model =
    let
        total =
            List.map .points model.plays |> List.sum
    in
        footer []
            [ div [] [ text "Total:" ]
            , div [] [ text <| toString total ]
            ]


viewPlayerForm : Model -> Html Msg
viewPlayerForm model =
    Html.form [ onSubmit Save ]
        [ input
            [ type_ "text"
            , placeholder "Add/Edit Player..."
            , onInput Input
            , value model.inputName
            ]
            []
        , button [ type_ "submit" ] [ text "Save" ]
        , button [ type_ "button", onClick Cancel ] [ text "Cancel" ]
        ]


viewPlaySection : Model -> Html Msg
viewPlaySection model =
    div []
        [ viewPlayListHeader
        , viewPlayList model
        ]


viewPlayListHeader : Html Msg
viewPlayListHeader =
    header []
        [ div [] [ text "Plays" ]
        , div [] [ text "Points" ]
        ]


viewPlayList : Model -> Html Msg
viewPlayList model =
    model.plays
        |> List.map viewPlay
        |> ul []


viewPlay : Play -> Html Msg
viewPlay play =
    li []
        [ i
            -- the red 'x' icon
            [ class "remove", onClick (DeletePlay play) ]
            []
        , div [] [ text play.name ]
        , div [] [ text <| toString play.points ]
        ]


view : Model -> Html Msg
view model =
    div [ class "scoreboard" ]
        [ h1 [] [ text "Score Keeper" ]
        , viewPlayerSection model
        , viewPlayerForm model
        , viewPlaySection model
        , div [ class "debug" ] [ text <| toString model ] {- places "debug" display below layout -}
        ]


main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initModel
        , view = view
        , update = update
        }
